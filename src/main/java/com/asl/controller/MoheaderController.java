package com.asl.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.asl.entity.Acsubview;
import com.asl.entity.Cacus;
import com.asl.entity.Caitem;
import com.asl.entity.Modetail;
import com.asl.entity.Moheader;
import com.asl.entity.ProjectStoreView;
import com.asl.entity.Xcodes;
import com.asl.enums.CodeType;
import com.asl.enums.TransactionCodeType;
import com.asl.model.ServiceException;
import com.asl.service.CacusService;
import com.asl.service.CaitemService;
import com.asl.service.MoheaderService;
import com.asl.service.ProjectStoreViewService;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Controller
@RequestMapping("/production/moheader")
public class MoheaderController extends ASLAbstractController{

	@Autowired private MoheaderService moheaderService;
	@Autowired private CaitemService caitemService;
	@Autowired private CacusService cacusService;
	@Autowired private ProjectStoreViewService projectStoreViewService;


	@GetMapping
	public String loadMoheaderPage(Model model) {
		model.addAttribute("moheader", getDefaultMoheader());
		model.addAttribute("prefix", xtrnService.findByXtypetrn(TransactionCodeType.BN_NUMBER.getCode(), Boolean.TRUE));
		model.addAttribute("moheaders", moheaderService.getAllMoheaders());
		model.addAttribute("caitemList", caitemService.getAllCaitems());
		model.addAttribute("customers", cacusService.getAllCacus("Customer"));

		Map<String, String> projectsMap = new TreeMap<>();
		List<ProjectStoreView> projects = projectStoreViewService.getProjectStores();
		for(ProjectStoreView p : projects) {
			if(StringUtils.isNotBlank(p.getXproject()) && projectsMap.get(p.getXproject()) == null) {
				projectsMap.put(p.getXproject(), p.getXprojectname());
			}
		}
		List<Xcodes> project = xcodesService.findByXtype(CodeType.PROJECT.getCode(), true);
		model.addAttribute("projects", project);
		model.addAttribute("selectedStores", Collections.emptyList());
		model.addAttribute("stores", Collections.emptyList());



		return"pages/production/moheader/moheader";
	}

	private Moheader getDefaultMoheader() {
		Moheader moheader = new Moheader();
		moheader.setXtrn(TransactionCodeType.BN_NUMBER.getdefaultCode());
		moheader.setXdate(new Date());
		moheader.setXrate(BigDecimal.ZERO);
		moheader.setXlineamt(BigDecimal.ZERO);
		moheader.setXstatus("Open");
		moheader.setXstatusjv("Open");
		moheader.setXstatusbom("Open");
		return moheader;
	}
	
	@GetMapping("/{xbatch}")
	public String loadMoheaderPage(@PathVariable String xbatch, Model model) {
		Moheader moheader = moheaderService.findMoheaderByXbatch(xbatch);
		if(moheader == null) return "rdirect:/production/moheader";

		model.addAttribute("moheader", moheader);
		model.addAttribute("prefix", xtrnService.findByXtypetrn(TransactionCodeType.BN_NUMBER.getCode(), Boolean.TRUE));
		model.addAttribute("moheaders", moheaderService.getAllMoheaders());


		List<Modetail> modetails = moheaderService.findModetailsByXbatch(xbatch);
		model.addAttribute("modetails", modetails);
		
		List<Caitem> caitemList = caitemService.getAllCaitems();
		model.addAttribute("caitemList", caitemList);
		
		List<Cacus> customers = cacusService.getAllCacus("Customer");
		model.addAttribute("customers", customers);
		
		Map<String, String> projectsMap = new TreeMap<>();
		List<ProjectStoreView> projects = projectStoreViewService.getProjectStores();
		for(ProjectStoreView p : projects) {
			if(StringUtils.isNotBlank(p.getXproject()) && projectsMap.get(p.getXproject()) == null) {
				projectsMap.put(p.getXproject(), p.getXprojectname());
			}
		}
		
		List<Xcodes> project = xcodesService.findByXtype(CodeType.PROJECT.getCode(), true);
		model.addAttribute("projects", project);
		List<ProjectStoreView> selectedStores = projectStoreViewService.getProjectStoresByXproject(moheader.getXhwh());
		model.addAttribute("selectedStores", selectedStores);
		
		// Store list
		List<ProjectStoreView> list = projectStoreViewService.getProjectStoresByXtype(moheader.getXhwh());
		list.sort(Comparator.comparing(ProjectStoreView::getXcode));
		model.addAttribute("stores", list);
		
		return"pages/production/moheader/moheader";
	}
	
	@GetMapping("/preloadeddata")
	public @ResponseBody PreData getItemList(){
		PreData preData = new PreData();

		List<Caitem> caitemList = caitemService.getAllCaitems();
		if(caitemList != null && !caitemList.isEmpty()) {
			caitemList.sort(Comparator.comparing(Caitem::getXitem));
			preData.setCaitemList(caitemList);
		}
	
		List<Cacus> customers = cacusService.getAllCacus("Customer");
		if(customers != null && !customers.isEmpty()) {
			customers.sort(Comparator.comparing(Cacus::getXcus));
			preData.setCustomers(customers);
		}
		
		List<ProjectStoreView> projects = projectStoreViewService.getProjectStores();
		projects.sort(Comparator.comparing(ProjectStoreView::getXproject));
		preData.setStores(projects);

		return preData;
	}

	@Data
	class PreData {
		List<Caitem> caitemList = new ArrayList<>();
		List<Cacus> customers = new ArrayList<>();
		List<ProjectStoreView> stores = new ArrayList<>();
	}

	@PostMapping(value = "/savebatch" , headers="Accept=application/json")
	public @ResponseBody Map<String, Object> save(@RequestBody String json){

		batchMaster vm = new batchMaster();

		ObjectMapper obm = new ObjectMapper();
		obm.setDateFormat(SDF2);
		try {
			vm = obm.readValue(json, batchMaster.class);
			System.out.println(vm.toString());
		} catch (JsonProcessingException e) {
			log.error(ERROR, e.getMessage(), e);
			responseHelper.setErrorStatusAndMessage(e.getMessage());
			return responseHelper.getResponse();
		}

		Moheader moheader = vm.getMoheader();
		List<Modetail> modetails = vm.getModetails();

		try {
			return moheaderService.saveWithDetail(moheader, modetails, responseHelper);
		} catch (ServiceException e) {
			log.error(ERROR, e.getMessage(), e);
			responseHelper.setErrorStatusAndMessage(e.getMessage());
			return responseHelper.getResponse();
		}
	}

	@PostMapping("/delete/{xbatch}")
	public @ResponseBody Map<String, Object> delete(@PathVariable String xbatch){
		return deleteBatchWithDetails(xbatch, true);
	}

	private Map<String, Object> deleteBatchWithDetails(String xbatch, boolean archive){
		Moheader moheader = moheaderService.findMoheaderByXbatch(xbatch);
		if(moheader == null) {
			responseHelper.setErrorStatusAndMessage("Batch not found in this system");
			return responseHelper.getResponse();
		}

		List<Modetail> modetails = moheaderService.findModetailsByXbatch(xbatch);
		if(modetails != null && !modetails.isEmpty()) {
			// delete all batch details first
			for(Modetail acd : modetails) {
				moheaderService.deleteModetail(acd.getXrow(), acd.getXbatch());
			}
		}

		long count = moheaderService.deleteMoheader(moheader.getXbatch());
		if(count == 0) {
			responseHelper.setErrorStatusAndMessage("Can't delete Batch");
			return responseHelper.getResponse();
		}

		responseHelper.setSuccessStatusAndMessage("Batch deleted successfully");
		responseHelper.setRedirectUrl("/production/moheader");
		return responseHelper.getResponse();
	}

}

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
class batchMaster{
	private Moheader moheader;
	private List<Modetail> modetails;
}
