package com.asl.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.asl.entity.Modetail;
import com.asl.entity.Moheader;

@Mapper
public interface MoheaderMapper {

	// For Header
	public long saveMoheader(Moheader moheader);

	public long updateMoheader(Moheader moheader);

	public long deleteMoheader(String xbatch, String zid);

	public List<Moheader> getAllMoheaders(String zid);

	public Moheader findMoheaderByXbatch(String xbatch, String zid);

	// For Detail
	public long saveModetail(Modetail modetail);

	public long updateModetail(Modetail mobomdetail);

	public long deleteModetail(int xrow, String xbatch, String zid);

	public List<Modetail> getAllModetail(String zid);

	public Modetail findModetailByXrowAndXbatch(int xrow, String xbatch, String zid);

	public List<Modetail> findModetailsByXbatch(String xbatch, String zid);

}
