package com.asl.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.asl.entity.Modetail;
import com.asl.entity.Moheader;
import com.asl.model.ResponseHelper;
import com.asl.model.ServiceException;

@Component
public interface MoheaderService {

	// For Header
	public long saveMoheader(Moheader moheader);

	public long updateMoheader(Moheader moheader);

	public Map<String, Object> saveWithDetail(Moheader moheader, List<Modetail> modetail, ResponseHelper responseHelper) throws ServiceException;

	public long deleteMoheader(String xbatch);

	public List<Moheader> getAllMoheaders();

	public Moheader findMoheaderByXbatch(String xbatch);

	// For Detail
	public long saveModetail(Modetail modetail);

	public long updateModetail(Modetail mobomdetail);

	public long deleteModetail(int xrow, String xbatch);

	public List<Modetail> getAllModetail();

	public Modetail findModetailByXrowAndXbatch(int xrow, String xbatch);

	public List<Modetail> findModetailsByXbatch(String xbatch);

}
