package com.asl.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asl.entity.Acdef;
import com.asl.entity.Acdetail;
import com.asl.entity.Acheader;
import com.asl.entity.Mobomdetail;
import com.asl.entity.Mobomheader;
import com.asl.entity.Modetail;
import com.asl.entity.Moheader;
import com.asl.mapper.MobomheaderMapper;
import com.asl.mapper.MoheaderMapper;
import com.asl.model.ResponseHelper;
import com.asl.model.ServiceException;
import com.asl.service.MobomheaderService;
import com.asl.service.MoheaderService;
@Service
public class MoheaderServiceImpl  extends AbstractGenericService implements MoheaderService{

	@Autowired private MoheaderMapper  moheaderMapper;

	@Transactional
	@Override
	public long saveMoheader(Moheader moheader) {
		if(moheader == null) return 0;
		moheader.setZid(sessionManager.getBusinessId());
		moheader.setZauserid(getAuditUser());
		return moheaderMapper.saveMoheader(moheader);
	}

	@Override
	public Map<String, Object> saveWithDetail(Moheader moheader, List<Modetail> modetails,
			ResponseHelper responseHelper) throws ServiceException {
		if(moheader == null) throw new ServiceException("Data not found to save");

		// save or update Moheader first
		// if existing
		if(StringUtils.isNotBlank(moheader.getXbatch())) {
			Moheader exist = findMoheaderByXbatch(moheader.getXbatch());
			if(exist == null) throw new ServiceException("Can't find batch in this system");

			BeanUtils.copyProperties(moheader, exist, "xbatch","xtrn");
			long count = updateMoheader(exist);
			if(count == 0) throw new ServiceException("Can't update batch");
			else responseHelper.setSuccessStatusAndMessage("Batch updated successfully");
			
		} else {
			long count = saveMoheader(moheader);
			if(count == 0) throw new ServiceException("Can't create Batch");
			else responseHelper.setSuccessStatusAndMessage("Batch created successfully");
		}

		// now save details if exist
		if(modetails == null || modetails.isEmpty()) {
//			responseHelper.setSuccessStatusAndMessage("BOM created successfully");
			responseHelper.setRedirectUrl("/production/moheader/" + moheader.getXbatch());
			return responseHelper.getResponse();
		}

		// delete existings
		List<Modetail> existingDetails = findModetailsByXbatch(moheader.getXbatch());
		if(existingDetails != null && !existingDetails.isEmpty()) {
			for(Modetail acd : existingDetails) {
				long count = deleteModetail(acd.getXrow(), acd.getXbatch());
				if(count == 0) throw new ServiceException("Can't delete existing details");
			}
		}

		for(Modetail modetail : modetails) {
			// set Batch header ref
			modetail.setXbatch(moheader.getXbatch());

			if(StringUtils.isBlank(modetail.getXitem())) throw new ServiceException("Item required");
			if(modetail.getXqty().compareTo(BigDecimal.ZERO) == 0) {
				throw new ServiceException("Qty must be greater than 0");
			}

			// if existing
			if(modetail.getXrow() > 0) {
				Modetail exist = findModetailByXrowAndXbatch(modetail.getXrow(), modetail.getXbatch());
				if(exist == null) throw new ServiceException("Can't find detail to do update");

				BeanUtils.copyProperties(modetail, exist, "xbatch", "xrow");
				long count = updateModetail(exist);
				if(count == 0) throw new ServiceException("Can't update batch detail");
			} else {
				long count = saveModetail(modetail);
				if(count == 0) throw new ServiceException("Can't save BOM detail");
			}

		}

//		responseHelper.setSuccessStatusAndMessage("Batch created successfully");
		responseHelper.setRedirectUrl("/production/moheader/" + moheader.getXbatch());
		return responseHelper.getResponse();
	}

	@Transactional
	@Override
	public long updateMoheader(Moheader moheader) { 
		if(moheader == null) return 0;
		moheader.setZid(sessionManager.getBusinessId());
		moheader.setZauserid(getAuditUser());
		return moheaderMapper.updateMoheader(moheader);
	}

	@Transactional
	@Override
	public long deleteMoheader(String xbatch) {
		if(StringUtils.isBlank(xbatch)) return 0;
		return moheaderMapper.deleteMoheader(xbatch, sessionManager.getBusinessId());
	}

	@Override
	public List<Moheader> getAllMoheaders() {
		return moheaderMapper.getAllMoheaders(sessionManager.getBusinessId());
	}

	@Override
	public Moheader findMoheaderByXbatch(String xbatch) {
		return moheaderMapper.findMoheaderByXbatch(xbatch,sessionManager.getBusinessId());
	}

	//Detail
	
	@Transactional
	@Override
	public long saveModetail(Modetail modetail) {
		if(modetail == null) return 0;
		modetail.setZid(sessionManager.getBusinessId());
		modetail.setZauserid(getAuditUser());
		return moheaderMapper.saveModetail(modetail);
	}

	@Transactional
	@Override
	public long updateModetail(Modetail modetail) {
		if(modetail == null) return 0;
		modetail.setZid(sessionManager.getBusinessId());
		modetail.setZauserid(getAuditUser());
		return moheaderMapper.updateModetail(modetail);
	}

	@Transactional
	@Override
	public long deleteModetail(int xrow, String xbatch) {
		if(xrow == 0 || StringUtils.isBlank(xbatch)) return 0;
		return moheaderMapper.deleteModetail(xrow, xbatch, sessionManager.getBusinessId());
	}

	@Override
	public List<Modetail> getAllModetail() {
		return moheaderMapper.getAllModetail(sessionManager.getBusinessId());
	}

	@Override
	public Modetail findModetailByXrowAndXbatch(int xrow, String xbatch) {
		if(xrow == 0 || StringUtils.isBlank(xbatch)) return null;
		return moheaderMapper.findModetailByXrowAndXbatch(xrow, xbatch, sessionManager.getBusinessId());
	}

	@Override
	public List<Modetail> findModetailsByXbatch(String xbatch) {
		if(StringUtils.isBlank(xbatch)) return null;
		return moheaderMapper.findModetailsByXbatch(xbatch, sessionManager.getBusinessId());
	}


}
